let __delayFunctionInstances = {};
function delayFunction(instanceName, delay, callback) {
    if (typeof callback != 'function') {
        throw "Callback must be a function";
    }

    if (__delayFunctionInstances[instanceName] === undefined) {
        __delayFunctionInstances[instanceName] = null;
    }

    if (__delayFunctionInstances[instanceName] !== null) {
        clearTimeout(__delayFunctionInstances[instanceName]);
    }

    __delayFunctionInstances[instanceName] = setTimeout(callback, delay);
}
