const RedisJob = (function() {
    let rawElementRedisModal = '' +
        '<div class="modal slide-down disable-scroll" id="log-download" tabindex="-1" role="dialog" aria-hidden="false">\n' +
        '    <div class="modal-dialog modal-lg">\n' +
        '        <div class="modal-content-wrapper">\n' +
        '            <div class="modal-content">\n' +
        '                <div class="modal-header">\n' +
        '                    <h4 class="modal-title text-white bold">Log Download</h4>\n' +
        '                </div>\n' +
        '                <div class="content-loading">\n' +
        '                    <div class="modal-body text-center">\n' +
        '                        <div class="fa fa-circle-o-notch fa-spin fa-2x"></div>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '                <div class="content-main" style="display: none">\n' +
        '                    <div class="modal-body">\n' +
        '                        <pre class="code"></pre>\n' +
        '                    </div>\n' +
        '                    <div class="modal-footer">\n' +
        '                        <a href="javascript://" class="btn btn-primary start-progress">Generate</a>\n' +
        '                        <a href="javascript://" class="btn btn-success start-download" download>\n' +
        '                            <i class="fa fa-file-excel-o"></i>\n' +
        '                            Download\n' +
        '                        </a>\n' +
        '                    </div>\n' +
        '                </div>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </div>\n' +
        '</div>';

    let _modal = $(rawElementRedisModal);
    let _url_redis = main_url_redis;
    let _private = {
        'config': {},
        'interval': null
    };

    function loadModal(dataModal) {
        _modal.find(".content-main .code").html(dataModal.message);
        if (dataModal.in_progress) {
            _modal.find(".content-main .start-progress").hide();
        } else {
            _modal.find(".content-main .start-progress").show();
        }

        if (dataModal.finish) {
            _modal.find(".content-main .start-download").show();
        } else {
            _modal.find(".content-main .start-download").hide();
        }
    }

    async function refreshMessage() {
        let pProgress = await fetch(`${_url_redis}/progress`, {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({"name": reportName()})
        });
        let dProgress = await pProgress.json();

        loadModal(dProgress.data);
        if (dProgress.data.in_progress) {
            _private.interval = setTimeout(refreshMessage, 500);
        }

        return true;
    }

    function reportName() {
        let temporal = _private.config.name;
        if (typeof temporal === 'function') {
            return temporal();
        }

        return temporal;
    }

    function redisData() {
        let temporal = _private.config.data;
        if (typeof temporal === 'function') {
            return temporal();
        }

        return temporal;
    }

    function prepareModal() {
        _private.config.button.on('click', async function(e) {
            e.preventDefault();
            _modal.modal('show');
        })

        _modal.appendTo($("body"));
        _modal.attr('id', `log_download_redis`)
        _modal.find(".start-download").on('click', async function(e) {
            e.preventDefault();
            let _self = $(this);
            let downloadParam = $.param({name: reportName()});
            let downloadUrl = `${_url_redis}/download?${downloadParam}`;

            _self.button('loading');
            let reqDownload = await fetch(downloadUrl, {
                method: 'OPTIONS',
            });
            let resDownload = await reqDownload.json();

            _self.button('reset');
            if (resDownload.ready === false) {
                alert("Report file is not ready");
            } else {
                window.location.href = downloadUrl;
            }
        });

        _modal.on('show.bs.modal', async function() {
            await refreshMessage();
            _modal.find('.content-loading').hide();
            _modal.find('.content-main').show();
        });
        _modal.on('hidden.bs.modal', function() {
            _modal.find('.content-loading').show();
            _modal.find('.content-main').hide();
            _modal.find(".content-main .code").html('');
            _modal.find(".content-main .start-progress").show();
            _modal.find(".content-main .start-download").hide();
            if (_private.interval !== null) {
                clearTimeout(_private.interval);
            }
        });

        _modal.find(".content-main .start-progress").on('click', async function(e) {
            e.preventDefault();
            let _self = $(this);
            _self.button('loading');

            let pProgress = await fetch(`${_url_redis}/job`, {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify({
                    name: reportName(),
                    data: redisData()
                })
            });

            loadModal({
                message: "Loading message....",
                in_progress: true,
                finish: false,
            })

            setTimeout(refreshMessage, 1500);
            _self.button('reset');
        });
    }

    function RedisJob(button, name, data) {
        let main = Object.create(RedisJob.prototype);
        _private.config = {
            name: name,
            data: data,
            button: null
        }

        let eButton = $(button);
        if (eButton.length === 0) {
            console.error("Button element not found");
            return;
        }

        _private.config.button = eButton;
        prepareModal();
        return main;
    }

    RedisJob.prototype.getConfig = function() {
        return _private['config'];
    }

    return RedisJob;
}());
