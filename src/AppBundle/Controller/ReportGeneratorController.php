<?php

namespace AppBundle\Controller;

use AppBundle\Helpers\ReportGenerator;
use AppBundle\RedisJob\Report\DownloadJson;
use AppBundle\Services\Redis\RedisJobAbstract;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;



/**
 * @Route("/report/generator")
 */
class ReportGeneratorController extends Controller
{
    /**
     * @Route("", name="report_generator_core")
     * @Method({"GET"})
     */
    public function coreUrl()
    {
        return $this->json(['success' => true]);
    }
    
    /**
     * @Route("/job", name="report_generator_job")
     * @Method({"POST"})
     */
    public function assignJob(Request $req)
    {
        $post = $req->request;
        $report = $post->get('name');
        $report_data = $post->get('data');

        $dictionary = [
            DownloadJson::$name . "/generate" => DownloadJson::class,
        ];
        
        $classReport = $dictionary[$report] ?? null;
        if ($classReport === null or !is_a($classReport, RedisJobAbstract::class, true)) {
            return $this->json(['success' => false, 'message' => 'Report not found'], 404);
        }
        
        $reportSplit = explode("/", $report);
        $functionName = end($reportSplit);
        
        $redis_job = $this->container->get('redis_job');
        $report_data['user_id'] = $this->getUser()->getId();
        $detailJob = $classReport::getJob($store->getId(), $functionName, $report_data);
        $redis_job->pushJob($detailJob);
        
        return $this->json(['success' => true, 'message' => 'Successfully start generate report']);
    }
    
    /**
     * @Route("/download", name="report_generator_download")
     * @Method({"GET", "OPTIONS"})
     */
    public function downloadReport(Request $req)
    {
        $name = $req->query->get("name");
        var_dump($name);exit;
        $progress = new ReportGenerator($this->container, $name, false);
        if ($req->isMethod('OPTIONS')) {
            return $this->json(['success' => true, 'ready' => $progress->isFinish()]);
        } else {
            if ($progress->isFinish()) {
                $masterName = [
                    DownloadJson::$name . "/generate" => 'DownloadJson',

                ];

                $filename = $masterName[$name] ?? "Unknown";
                $fn_date = date("Ymd");
                $param = @json_decode(file_get_contents($progress->getParameterFile()), true);
                if (isset($param['dateStart']) and isset($param['dateEnd'])) {
                    $meta_name = array_map(function ($data) {
                        return str_replace("-", "", $data);
                    }, [$param['dateStart'], $param['dateEnd']]);
                    $fn_date = implode("_", $meta_name);
                }
                
                [$cType, $ext] = $progress->getExtension();
                header("Content-Type: {$cType}");
                header("Content-Disposition: attachment; filename=Laporan{$filename}_{$fn_date}.{$ext}");
                header("Cache-Control: no-cache, no-store, must-revalidate");
                header("Pragma: no-cache");
                header("Expires: 0");
                
                echo file_get_contents($progress->getFinalFile());
                exit;
            }
            
            throw new NotFoundHttpException("Your report is not ready");
        }
    }
    
    /**
     * @Route("/progress", name="report_generator_progress")
     * @Method({"POST"})
     */
    public function getProgress(Request $req)
    {
        $params = $req->request;
        $nameGenerator = $params->get('name');
        var_dump($params);
        var_dump($nameGenerator);exit;
        if ($nameGenerator == "") {
            return false;
        }
        
        $progress = new ReportGenerator($this->container, $nameGenerator, false);
        $message = [
            'success' => true,
            'data' => [
                'message' => $progress->getProgress(),
                'in_progress' => $progress->isInProgress(),
                'finish' => $progress->isFinish()
            ],
        ];
        
        return $this->json($message);
    }
}
