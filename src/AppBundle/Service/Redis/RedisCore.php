<?php

namespace AppBundle\Services\Redis;

class RedisCore
{
    /** @var \AppBundle\Services\Redis\RedisConfig */
    private $_config;
    /** @var \Redis */
    private $_instance;
    
    public function __construct(RedisConfig $config)
    {
        $this->_config = $config;
        $this->_instance = new \Redis();
        $conack = $this->_instance->pconnect($config->getHost(), $config->getPort());
        if ($conack === false) {
            throw new \InvalidArgumentException("Can't connect to redis server");
        }
        
        $this->_instance->select($config->getDb());
        if ($config->getNamespace() === null or $config->getNamespace() == "") {
            throw new \InvalidArgumentException("[REDIS] Namespace required");
        }
    }
    
    public function getConfig()
    {
        return $this->_config;
    }
    
    public function getInstance()
    {
        return $this->_instance;
    }
    
    public function pushJob(RedisJob $job)
    {
        $named_job = "{$this->_config->getPrefix()}/job/block/{$job->block_id}";
        return $this->_instance->lPush($named_job, $job->json());
    }
    
    public function getJob($block_id): ?RedisJob
    {
        $result = null;
        $named_job = "{$this->_config->getPrefix()}/job/block/{$block_id}";
        $job = $this->_instance->rPop($named_job);
        if ($job !== false) {
            $data = json_decode($job, true);
            $result = RedisJob::fromJson($data);
        }
        
        return $result;
    }
}
