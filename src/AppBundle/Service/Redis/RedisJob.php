<?php

namespace AppBundle\Services\Redis;

class RedisJob
{
    public $name;
    public $command;
    public $data = [];
    
    /** @var int
     *  Which block this job will be done
     */
    public $block_id = 0;
    
    public function __construct($name, $command)
    {
        $this->name     = $name;
        $this->command = $command;
    }
    
    public function getClassName()
    {
        $polishedTopic = str_replace("/", "\\", $this->name);
        $cleaned = preg_replace_callback('/(^|_|\\\\)+(.)/', function ($match) {
            return ('_' === $match[1] ? '' : $match[1]).strtoupper($match[2]);
        }, $polishedTopic);
        
        return "{$cleaned}Job";
    }
    
    public function getFunctionName()
    {
        return "do" . preg_replace_callback('/(^|_)+(.)/', function ($match) {
                return ('_' === $match[1] ? '' : $match[1]) . strtoupper($match[2]);
            }, $this->command);
    }
    
    public static function fromJson($json): RedisJob
    {
        $cls = new self($json['name'], $json['command']);
        $cls->block_id = $json['block_id'] ?? 0;
        $cls->data = $json['data'] ?? [];
        
        return $cls;
    }
    
    public function json()
    {
        return json_encode([
            'name' => $this->name,
            'command' => $this->command,
            'data' => $this->data,
            'block_id' => $this->block_id,
        ]);
    }
}
