<?php

namespace AppBundle\Services\Redis;

use Symfony\Component\DependencyInjection\ContainerInterface;

class RedisService
{
    private $_logger;
    private $_isDebug;
    private $_instance;
    private $_container;
    
    public function __construct(ContainerInterface $container)
    {
        $config = $container->getParameter("redis_job");
        
        $this->_container = $container;
        $this->_isDebug = $container->get('kernel')->isDebug();
        $this->_logger = $container->get('logger');
        $this->_instance = new RedisCore(new RedisConfig($config));
    }
    
    public function getInstance()
    {
        return $this->_instance;
    }
    
    public function pushJob(RedisJob $job)
    {
        return $this->_instance->pushJob($job);
    }
    
    public function popJob($block_id)
    {
        return $this->_instance->getJob($block_id);
    }
    
    public function execute(RedisJob $job)
    {
        $config = $this->_instance->getConfig();
        $className = "{$config->getNamespace()}\\{$job->getClassName()}";
        
        if (!class_exists($className)) {
            $this->_logger->error(" [RDSJOB] [ERROR] Class {$className} is not exist");
            throw new \Exception("Class {$className} is not exist");
        }
        
        $instance = new $className($this->_container, $job);
        if (!($instance instanceof RedisJobAbstract)) {
            $this->_logger->error(" [RDSJOB] [ERROR] Class {$className} must be instance of ".RedisJobAbstract::class);
            throw new \Exception("Class {$className} must be instance of ".RedisJobAbstract::class);
        }
        
        if ($this->_isDebug) $this->_logger->info(" [>] RUN script {$className}");
        $instance->execute();
        if ($this->_isDebug) $this->_logger->info(" [V] Done");
        
        return true;
    }
}
