<?php

namespace AppBundle\Services\Redis;

use AppBundle\Entity\Staff\Staff;
use AppBundle\Entity\Store;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

abstract class RedisJobAbstract
{
    /** @var \AppBundle\Services\Redis\RedisJob */
    private $_job;
    /** @var \Redis */
    private $_redis;
    /** @var \Doctrine\Bundle\DoctrineBundle\Registry */
    private $_doctrine;
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    private $_container;
    /** @var \Symfony\Bridge\Monolog\Logger[] */
    private $_logger = [];
    
    public static $name;
    protected static $block_id = 0;
    
    public function __construct(ContainerInterface $container, RedisJob $job)
    {
        $this->_job = $job;
        $this->_container = $container;
        $this->_redis = $container->get('redis_job');
        $this->_doctrine = $container->get('doctrine');

    }
    
    public function getRedis()
    {
        return $this->_redis;
    }
    
    public function getDoctrine()
    {
        return $this->_doctrine;
    }
    
    public function getContainer()
    {
        return $this->_container;
    }
    
    public function getData()
    {
        return new ParameterBag($this->_job->data ?? []);
    }
    
    public function getLogger($name)
    {
        if (isset($this->_logger[$name])) {
            return $this->_logger[$name];
        }
        
        $kernel = $this->_container->get('kernel');
        $log_path = $kernel->getLogDir() . "/" . $kernel->getEnvironment();
        $formatter = new JsonFormatter(JsonFormatter::BATCH_MODE_NEWLINES);
        $formatter->includeStacktraces();
        
        $fn = "{$log_path}_{$name}.log";
        $handler = new StreamHandler($fn, Logger::DEBUG, true, 0664);
        $handler->setFormatter($formatter);
        
        return $this->_logger[$name] = new Logger("json-{$name}-log", [$handler]);
    }
    
    public function handleError(\Exception $e)
    {
        // do nothing
    }
    
    final public static function getJob($command, $data = [])
    {
        $init = new RedisJob(static::$name, $command);
        $init->block_id = static::$block_id;
        $init->data = $data;
        return $init;
    }
    
    public function execute()
    {
        /** @var \Doctrine\DBAL\Connection $connection */
        $connection = $this->_doctrine->getConnection();
        try {
            $command = $this->_job->getFunctionName();
            if (!method_exists($this, $command)) {
                throw new \Exception("Class {$this->_job->getClassName()} has no command [{$command}]");
            }
            
            $connection->beginTransaction();
            $this->$command();
            $connection->commit();
        } catch (\Exception $e) {
            if ($connection->isTransactionActive())
                $connection->rollBack();
            
            $traces = [];
            $message = "[ERROR] [RDS] {$e->getMessage()}";
            foreach ($e->getTrace() as $trace) {
                $traces[] = "{$trace['file']}->{$trace['function']}::{$trace['line']}";
            }
            $this->getLogger('error')->error($message, [
                'exception' => get_class($e),
                'datetime' => round(microtime(true) * 1000),
                'file' => $e->getFile(),
                'line_no' => $e->getLine(),
                'trace' => $traces,
                'service' => "inventory.service.error"
            ]);
            
            $this->handleError($e);
            echo $message;
            exit(1);
        }
    }
}
