<?php

namespace AppBundle\Services\Redis;

class RedisConfig
{
    private $_config;
    public function __construct(array $_config)
    {
        $this->_config = $_config;
    }
    
    public function getHost()
    {
        return $this->_config['host'] ?? null;
    }
    
    public function getPort()
    {
        return $this->_config['port'] ?? 6379;
    }
    
    public function getDb()
    {
        return $this->_config['db'] ?? null;
    }
    
    public function getPrefix()
    {
        return $this->_config['prefix'] ?? null;
    }
    
    public function getNamespace()
    {
        return $this->_config['namespace'] ?? null;
    }
}
