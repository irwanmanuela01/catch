<?php

namespace AppBundle\Helpers;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ReportGenerator
{
    private $_name;
    private $_storeId;
    private $_cacheDir;
    
    const EXPIRED = 1800;
    
    public function __construct(ContainerInterface $container, $name, $pushSignal = true)
    {
        $kernel = $container->get('kernel');
        $this->_cacheDir = $kernel->getCacheDir();
        $this->_name = $name;
        
        $this->checkAlive();
    }
    
    private function getProgressFile()
    {
        $path = "{$this->getDir()}/progress.prg";
        if (!is_file($path)) {
            touch($path);
            chmod($path, 0775);
        }
        return $path;
    }
    
    public function getParameterFile()
    {
        $path = "{$this->getDir()}/parameter.json";
        if (!file_exists($path)) {
            touch($path);
            chmod($path, 0775);
        }
        return $path;
    }
    
    public function getFinalFile()
    {
        $path = "{$this->getDir()}/final";
        if (!is_file($path)) {
            touch($path);
            chmod($path, 0775);
        }
        
        return $path;
    }
    
    public function getExtensionFile()
    {
        return $this->getFinalFile() . "_ext";
    }
    
    public function getDir()
    {
        $named = preg_replace('/[^A-Z0-9]+/i', '_', strtolower($this->_name));
        $dir = $this->_cacheDir."/../report/{$this->_storeId}/{$named}";
        if (!is_dir($dir)) mkdir($dir, 0775, true);
        
        return $dir;
    }
    
    public function isInProgress(): bool
    {
        $file = $this->getProgressFile();
        if (!file_exists($file)) {
            return false;
        }
        
        $content = file_get_contents($file);
        return $content != "";
    }
    
    public function isFinish(): bool
    {
        return file_exists($this->getProgressFile() . ".fn");
    }
    
    public function getProgress(): string
    {
        $prg = $this->getProgressFile();
        return file_get_contents(is_file("{$prg}.fn") ? "{$prg}.fn" : $prg);
    }
    
    public function getTopic(): string
    {
        return "{$this->_storeId}/ws/{$this->_name}";
    }
    
    public function updateParams(array $data)
    {
        file_put_contents($this->getParameterFile(), json_encode($data));
    }
    
    public function update(string $progress)
    {
        $file = $this->getProgressFile();
        $progress = date("[Y-m-d H:i:s]") . " {$progress}";
        file_put_contents($file, $progress . PHP_EOL, FILE_APPEND);
    }
    
    public function finish()
    {
        if (is_file($this->getFinalFile())) chmod($this->getFinalFile(), 0775);
        rename($this->getProgressFile(), $this->getProgressFile() . ".fn");
    }
    
    public function reset()
    {
        foreach (scandir($this->getDir()) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            
            $path = $this->getDir() . DIRECTORY_SEPARATOR . $item;
            if (file_exists($path)) unlink($path);
        }
        
        rmdir($this->getDir());
    }
    
    public function checkAlive()
    {
        $file = $this->getFinalFile();
        $fileTime = time() - filemtime($file);
        if ($fileTime >= self::EXPIRED) $this->reset();
    }
    
    public function setExtension($ext)
    {
        file_put_contents($this->getExtensionFile(), $ext);
    }
    
    public function stringify($string)
    {
        return '="' . str_replace('"', '""', $string) . '"';
    }
    
    public function getExtension()
    {
        $contentType = "plain/text";
        $ext = file_get_contents($this->getExtensionFile());
        
        if ($ext == 'zip') {
            $contentType = 'application/zip';
        } elseif ($ext == 'xlsx') {
            $contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        } elseif ($ext == 'csv') {
            $contentType = "text/csv";
        }
        
        return [$contentType, $ext];
    }
}
