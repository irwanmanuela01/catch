<?php

namespace AppBundle\Command;

use \AppBundle\Services\Redis\RedisJob;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class JobRedisReceiverCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName("job:redis")
            ->addOption("filename", 'f', InputOption::VALUE_OPTIONAL, "payload file")
            ->addOption('block', 'b', InputOption::VALUE_OPTIONAL, "Block ID")
            ->setDescription("Redis job receiver");
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filename = $input->getOption('filename');
        $block_id = $input->getOption('block');
        if ($filename === null and $block_id === null) {
            $output->writeln("Required one of these options: filename or block");
            return;
        }
        
        $redis = $this->getContainer()->get('redis_job');
        if ($filename !== null) {
            if (!is_file($filename)) {
                throw new \InvalidArgumentException("File is missing");
            }
            
            $rawContent = json_decode(file_get_contents($filename), true);
            $job = RedisJob::fromJson($rawContent);
            $redis->execute($job);
            $output->writeln("[DONE] [{$job->block_id}] {$job->getClassName()}::{$job->getFunctionName()} {$filename}");
        }
        
        if ($block_id !== null) {
            $output->writeln("[RUN] block_id = {$block_id}");
            do {
                $job = $redis->popJob($block_id);
                if ($job === null) continue;

                $output->writeln("[RUN] [{$job->block_id}] {$job->getClassName()}::{$job->getFunctionName()}");
                $redis->execute($job);
                $output->writeln("      [>] [{$job->block_id}] {$job->getClassName()}::{$job->getFunctionName()}");
                $output->writeln("      [M] {$job->json()}");
            } while ($job !== null);

            $output->writeln("[FINISH]");
        }
    }
}
