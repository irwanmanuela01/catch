<?php

namespace AppBundle\RedisJob\Report;

use AppBundle\Helpers\ReportGenerator as ReportHelper;
use AppBundle\RedisJob\Base\ReportGenerator;
use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\ParameterBag;

class DownloadJson extends ReportGenerator
{
    public static $name = "report/download_json";
    public function doGenerate()
    {
        $params = $this->getData();
        var_dump($params);exit;

        $progress = new ReportHelper($this->getContainer(), self::$name . '/generate', false);
        $this->_progress = $progress;
        
        if ($progress->isInProgress()) {
            $progress->update("Download report in progress");
            return false;
        }
        
        $progress->reset();
        $progress->update("INIT process report");
        $progress->updateParams($this->getData()->all());
        
        $progress->update("Start Date = {$params->get('dateStart')}");
        $progress->update("End Date = {$params->get('dateEnd')}");
        $progress->update("Begin to get data");
        
        //header data on csv
        $tmpHead = [
            'Bound Number',
            'Kode SO',
            'No SO',
            'Created At',
            'Created By',
            'Type Transaction',
            'Customer Name',
            'Customer Email',
            'Customer Phone',
            'Customer Address',
            'Notes',
            'SKU',
            'Name',
            'Qty',
            'Meta1',
            'Meta2'
        ];
        
        if ($staff->getPermission()->isAllowReportHkmOutbound()) {
            array_push($tmpHead,
            'Phone',
            'Alias 1',
            'Alias 2',
            'Alias 3',
            'Alias 4');
        }

        $widthModifier = [11 => 15, 12 => 15, 8 => 30];
        $alignments = [
            [
                'cols' => [
                    [1, 3],
                    [6, 7], 9,
                    [11, 14]
                ],
                'alignment' => [
                    'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                ]
            ]
        ];
        
        $page = 0;
        $limit = 1000;
        $resultData = [];
        
        $params->set('limit', $limit);
        do {
            $paging = ($page + 1) * $limit;
            $params->set('offset', $page * $limit);
            $progress->update("Fetch {$paging} data");
            $tmpIns = $this->getMasterData($params, $store);
            $resultData = array_merge($resultData, $tmpIns);
            $page++;
        } while (count($tmpIns) >= $limit);
        
        $finalData = [];
        $csvContent = [];
        foreach ($resultData as $p) {
            $tmp_data = [
                $p['bound_number'],
                $p['kode_so'],
                $p['nomor_so'],
                $p['created_at'],
                $p['created_by'],
                $p['type_transaction'],
                $p['customer_name'],
                $p['customer_email'],
                $p['customer_phone'],
                $p['customer_address'],
                $p['notes'],
                $this->_progress->stringify($p['sku']),
                $p['name'],
                $p['qty'],
                $this->_progress->stringify($p['meta1']),
                $this->_progress->stringify($p['meta2'])
                
            ];

            if ($staff->getPermission()->isAllowReportHkmOutbound()) {
                array_push($tmp_data,
                    $this->_progress->stringify($p['phone']),
                    $p['alias_1'],
                    $p['alias_2'],
                    $p['alias_3'],
                    $p['alias_4']
                );
            }
            
            $csvContent[] = $tmp_data;
            if (count($csvContent) == self::LIMIT_PER_FILE) {
                $finalData[] = $csvContent;
                $csvContent = [];
            }
        }
        
        if (count($csvContent) > 0) $finalData[] = $csvContent;
        if (count($finalData) == 0) $finalData[] = [];
        
        $this->setHeader($tmpHead);
        $this->setAlignment($alignments);
        $this->setWidth($widthModifier);
        $this->finalizeData('Inbound Report', $finalData);
        
        $progress->update("Finish");
        $progress->finish();
    }
    
    public function getMasterData(ParameterBag $params, Store $store)
    {

        $dateStart = $params->get('dateStart', date("Y-m-d 00:00:00"));
        $dateEnd = $params->get('dateEnd', date("Y-m-d 23:59:59"));


        $rawQuery = "SELECT bound.bound_number, IFNULL(sell.sell_number, '-') as kode_so, IFNULL(sell.so_number, '-') as nomor_so, bound.created_at, bound.created_by, bound.status as type_transaction, 
          IFNULL(sell.customer_name, '-') as customer_name, IFNULL(sell.customer_email, '-') as customer_email, IFNULL(sell.customer_phone, '-') as customer_phone, IFNULL(sell.customer_address, '-') as customer_address,
          bound.notes, product_variant.sku, bound_items.name, (bound_items.qty *-1) as qty, meta1, meta2, store.phone, store.alias_1, store.alias_2, store.alias_3, store.alias_4
          FROM bound
          JOIN bound_items on bound.id = bound_items.bound_id
          LEFT JOIN sell on bound.sell_id = sell.id
          JOIN product_variant on product_variant.id = bound_items.variant_id
          JOIN store ON bound.store_id = store.id
          WHERE bound_items.removed_at is null and bound_items.qty != 0
          AND bound.store_id = :store_id AND bound.removed_at is null
          AND bound.discr = 'outbound'
          AND bound.status = 'SALES'
          AND (bound.bound_date BETWEEN :dateStart AND :dateEnd)
          AND (bound.bound_number LIKE :query OR sell.sell_number LIKE :query OR sell.so_number LIKE :query)
          ORDER BY bound.id, bound.created_at DESC
          LIMIT :offs, :limt";
        
        $filters = [
            'dateStart' => $dateStart,
            'dateEnd' => $dateEnd,
            'store_id' => $store->getId(),
            'offs' => $params->get('offset'),
            'limt' => $params->get('limit'),
            'query' => "%".$params->get('search', '')."%"
        ];
        
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $this->getDoctrine()->getConnection();
        $prep = $conn->executeQuery($rawQuery, $filters, [
            'store_ids' => Connection::PARAM_STR_ARRAY,
            'offs' => \PDO::PARAM_INT,
            'limt' => \PDO::PARAM_INT,
        ]);
        return $prep->fetchAll(\PDO::FETCH_ASSOC);
    }
}
